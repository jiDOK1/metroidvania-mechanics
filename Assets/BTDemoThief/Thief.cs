using UnityEngine;
using Panda;
using UnityEngine.AI;

public class Thief : MonoBehaviour
{
    // TODO: check if at Npc
    // TODO: mark Npc as beklaut, go away after stealing (Wander?)
    public Npc[] npcs;
    public bool isPoliceInSight;
    public bool isPoliceApproaching;
    public bool hasEnoughForDay;
    public bool arePocketsFull;
    public bool isNpcNear;
    public bool isDarkEnough;
    public int wallet;
    bool isAtNpc;
    bool isWandering;
    Npc nextNpc;

    NavMeshAgent agent;
    Light sun;

    void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
        sun = GameObject.FindObjectOfType<Light>();
    }

    void Start()
    {
        nextNpc = npcs[Random.Range(0, npcs.Length)];
    }

    void Update()
    {
        Ray ray = new Ray(transform.position, -sun.transform.forward);
        isDarkEnough = Physics.Raycast(ray, 500f);
    }

    [Task]
    public bool IsPoliceInSight()
    {
        return isPoliceInSight;
    }

    [Task]
    public bool IsPoliceApproaching()
    {
        return isPoliceApproaching;
    }

    [Task]
    public bool HasEnoughForDay()
    {
        return hasEnoughForDay;
    }

    [Task]
    public bool ArePocketsFull()
    {
        return arePocketsFull;
    }
    
    [Task]
    public bool IsAtNpc()
    {
        return isAtNpc;
    }

    [Task]
    public bool IsDarkEnough()
    {
        //somehow doesnt work here, only in Update()
        //Ray ray = new Ray(transform.position, sun.transform.forward);
        //isDarkEnough = !Physics.Raycast(ray, 500f);
        return isDarkEnough;
    }

    [Task]
    public void RunAway()
    {
        ThisTask.Succeed();
    }

    [Task]
    public void GoToHideout()
    {
        ThisTask.Succeed();
    }

    [Task]
    public void ResetMoney()
    {
        ThisTask.Succeed();
    }

    [Task]
    public void DepositMoney()
    {
        ThisTask.Succeed();
    }

    [Task]
    public void Sleep()
    {
        ThisTask.Succeed();
    }

    [Task]
    public void EnterStreet()
    {
        ThisTask.Succeed();
    }

    [Task]
    public void FindNextNpc()
    {
        if (nextNpc == null) { return; }
        while (nextNpc.burned)
        {
            nextNpc = npcs[Random.Range(0, npcs.Length)];
        }
        ThisTask.Succeed();
    }

    [Task]
    public void GoToNpc()
    {
        agent.SetDestination(nextNpc.transform.position);
        if (agent.remainingDistance < 2.1f)
        //if (agent.isStopped)
        {
            isAtNpc = true;
            ThisTask.Succeed();
        }
    }

    [Task]
    public void StealMoney()
    {
        wallet += nextNpc.StealMoney();
        isAtNpc = false;
        ThisTask.Succeed();
    }

    [Task]
    public void Wander()
    {
        //if (!isWandering)
        //{
        //    Vector3 randPos = Random.insideUnitSphere * 30;
        //    randPos = new Vector3(randPos.x, 0f, randPos.y);
        //    agent.SetDestination(randPos);
        //    isWandering = true;
        //}
        //if (agent.remainingDistance < 0.1f)
        //{
        //    isWandering = false;
        //    ThisTask.Succeed();
        //}
        ThisTask.Succeed();
    }
}
