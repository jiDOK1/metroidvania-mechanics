using UnityEngine;
using UnityEngine.AI;

public class PolicePatrol : MonoBehaviour
{
    // TODO: calculate path and custom move with smoothing
    NavMeshAgent agent;
    public Transform[] patrolPoints;
    int curIdx;

    void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
    }

    void Start()
    {
        agent.SetDestination(patrolPoints[0].position);
    }

    void Update()
    {
        if (agent.remainingDistance < 3f)
        {
            curIdx = (curIdx + 1) % patrolPoints.Length;
            agent.SetDestination(patrolPoints[curIdx].position);
        }
    }
}
