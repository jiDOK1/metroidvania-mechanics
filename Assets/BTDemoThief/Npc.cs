using UnityEngine;

public class Npc : MonoBehaviour
{
    public bool burned;
    int money;
        
    void Start()
    {
        money = Random.Range(0, 100);
    }

    public int StealMoney()
    {
        burned = true;
        int amount = money;
        money = 0;
        return amount;
    }
}
