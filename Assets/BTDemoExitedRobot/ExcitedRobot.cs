using UnityEngine;
using Panda;

public class ExcitedRobot : MonoBehaviour
{
    public GameObject player;
    public float gravity = 5f;
    public float terminalY = 5f;
    public float maxYspeed= 5f;
    public float jumpStrength = 5f;
    public float fov = 0.35f;
    string message;
    CharacterController cc;
    float yVelocity;
    float dot;

    void Awake()
    {
        cc = GetComponent<CharacterController>();
    }

    void Update()
    {
        yVelocity -= gravity * Time.deltaTime;
        yVelocity = Mathf.Clamp(yVelocity, -terminalY, maxYspeed);
        Vector3 velocity = Vector3.up * yVelocity;
        cc.Move(velocity * Time.deltaTime);
    }

    private void OnGUI()
    {
        GUILayout.Label(message);
        GUILayout.Label(dot.ToString());
    }

    [Task]
    public void Idle()
    {
        message = "idle...";
        ThisTask.Succeed();
    }

    [Task]
    public void Jump()
    {
        yVelocity = jumpStrength;
        message = "jumping!";
        ThisTask.Succeed();
    }

    [Task]
    public void WaitForGrounded()
    {
        if (cc.isGrounded)
        {
            ThisTask.Succeed();
        }
    }

    [Task]
    public bool CanSeePlayer()
    {
        bool playerObstructed = true;
        Vector3 dirToPlayer = player.transform.position - transform.position;
        dirToPlayer.Normalize();
        Ray ray = new Ray(transform.position, dirToPlayer);
        RaycastHit hitInfo;
        if (Physics.Raycast(ray, out hitInfo, 100f))
        {
            if (hitInfo.collider.CompareTag("Player"))
            {
                playerObstructed = false;
            }
        }
        dot = Vector3.Dot(dirToPlayer, transform.forward);

        return dot > 0.35 && !playerObstructed;
    }

    [Task]
    public bool IsGrounded()
    {
        return cc.isGrounded;
    }

}
