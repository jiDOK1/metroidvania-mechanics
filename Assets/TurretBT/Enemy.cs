using UnityEngine;

public class Enemy : MonoBehaviour
{
    public float health = 10f;

    public float GetHealth()
    {
        return health;
    }

    public void DealDamage(float amount)
    {
        health -= amount;
        if (health <= 0f)
        {
            health = 0f;
            GetComponent<Renderer>().material.color = Color.black;
        }
    }
}
