using UnityEngine;
using Panda;

public class Turret : MonoBehaviour
{
    public bool noAlarm;
    public bool enemySpotted;
    public bool enemyAlive;
    public float range = 7f;
    public bool enemyInRange;
    public float standbyDuration = 10f;
    public bool isActivated;
    float standbyTimer;
    float activationTimer;
    float shootTimer;
    string guiMsg;
    Enemy enemy;

    void OnTriggerEnter(Collider other)
    {
        Enemy e = other.GetComponent<Enemy>();
        if (e == null) { return; }
        enemy = e;
        enemySpotted = true;
    }

    void OnTriggerExit(Collider other)
    {
        Enemy e = other.GetComponent<Enemy>();
        if (e == null) { return; }
        enemy = null;
        enemySpotted = false;
    }

    void OnGUI()
    {
        GUILayout.Label(guiMsg);
    }

    [Task]
    public bool NoAlarm()
    {
        return noAlarm;
    }

    [Task]
    public void Offline()
    {
        guiMsg = "offline";
        ThisTask.Succeed();
    }

    [Task]
    public bool EnemySpotted()
    {
        return enemySpotted;
    }

    [Task]
    public bool EnemyAlive()
    {
        return enemy?.GetHealth() > 0;
    }

    [Task]
    public bool EnemyInRange()
    {
        if(enemy == null) { return false; }
        return Vector3.SqrMagnitude(transform.position - enemy.transform.position) < range * range;
    }

    [Task]
    public void ShootEnemy()
    {
        guiMsg = "shooting";
        shootTimer += Time.deltaTime;
        if (shootTimer >= 2f)
        {
            shootTimer = 0f;
            Debug.Log("shooting now");
            enemy.DealDamage(2f);
            ThisTask.Succeed();
        }
    }

    [Task]
    public void Standby()
    {
        guiMsg = "standby";
        standbyTimer += Time.deltaTime;
        if (enemySpotted)
        {
            ThisTask.Fail();
        }
        if (standbyTimer >= standbyDuration)
        {
            standbyTimer = 0f;
            ThisTask.Succeed();
        }
    }

    [Task]
    public void Activate()
    {
        if (activationTimer == 0)
        {
            GetComponent<Animator>().SetBool("isActivated", true);
            guiMsg = "activating";
        }
        activationTimer += Time.deltaTime;
        if (activationTimer >= 2f)
        {
            activationTimer = 0f;
            isActivated = true;
            ThisTask.Succeed();
        }
    }

    [Task]
    public void Deactivate()
    {
        if (activationTimer == 0)
        {
            GetComponent<Animator>().SetBool("isActivated", false);
            guiMsg = "deactivating";
        }
        activationTimer += Time.deltaTime;
        if (activationTimer >= 2f)
        {
            activationTimer = 0f;
            isActivated = false;
            ThisTask.Succeed();
        }
    }

    [Task]
    public bool IsActivated()
    {
        return isActivated;
    }
}
