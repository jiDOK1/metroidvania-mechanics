using UnityEngine;

public class GrapplingPull : CharacterState
{
    public LineRenderer rope;
    public Transform hook;

    public override void OnEnter()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        Animator anim = hook.parent.GetComponent<Animator>();
        if (anim != null)
        {
            MoveFinishedEvent mfe = anim.GetComponent<MoveFinishedEvent>();
            if(mfe == null) { Debug.LogError("did not find move finished event"); }
            mfe.onFinishedMoving += OnFinishedMoving;
            anim.SetTrigger("movestart");
        }
        else
        {
            Debug.LogError("did not find animator");
        }
        // platform ist parent von hook => animation starten
        //PullablePlatform platform = hook.parent.GetComponent<PullablePlatform>();
        //if (platform == null)
        //{
        //    Debug.LogError("pullable platform not found");
        //}
        //else
        //{
        //    // platform sagt bescheid, wenn animation komplett => SwitchState()
        //    platform.onFinishedMoving += OnFinishedMoving;
        //    platform.SetMoving(true);
        //}
    }

    public override void OnExit()
    {
        rope.enabled = false;
    }

    public override void OnUpdate()
    {
        Vector3[] positions = new[] { meshTransform.position, hook.position };
        rope.SetPositions(positions);
    }

    public void OnFinishedMoving()
    {
        stateMachine.SwitchState(stateMachine.jumpNRunState);
    }
}
