using UnityEngine;

public abstract class CharacterState : MonoBehaviour
{
    public GUIStyle style;
    protected Animator anim;
    protected CharacterStateMachine stateMachine;
    protected Transform meshTransform;
    protected CharacterController cc;

    public abstract void OnEnter();
    public abstract void OnExit();
    public abstract void OnUpdate();

    protected virtual void Awake()
    {
        anim = GetComponentInChildren<Animator>();
        stateMachine = GetComponent<CharacterStateMachine>();
        meshTransform = transform.Find("Capsule");
        cc = GetComponent<CharacterController>();
    }

    //void OnGUI()
    //{
    //    GUILayout.Label(xVelocity.ToString(), style);
    //    GUILayout.Label(yVelocity.ToString(), style);
    //}
}
