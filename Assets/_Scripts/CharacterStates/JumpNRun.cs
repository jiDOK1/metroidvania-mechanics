using UnityEngine;

public class JumpNRun : CharacterState
{
    // horizontal
    public float xAccel = 10f;
    public float xSpeedMax = 5f;
    public float xFriction = 10f;
    // vertical
    public float jumpHeight = 10f;
    public float ySpeedMax = 6f;
    public float gravity = 9.81f;
    public float terminalY = 55f;
    // helper
    public LayerMask mask;
    //public GUIStyle style;
    int jumpCounter;
    bool isFalling;
    bool isTouchingClimbable;

    //void OnTriggerEnter(Collider other)
    //{
    //    //Debug.Log(other.gameObject.name);
    //    if (other.gameObject.CompareTag("ClimbableWall"))
    //    {
    //        isTouchingClimbable = true;
    //    }
    //}

    //void OnTriggerExit(Collider other)
    //{
    //    if (other.gameObject.CompareTag("ClimbableWall"))
    //    {
    //        isTouchingClimbable = false;
    //    }
    //}

    //void OnControllerColliderHit(ControllerColliderHit hit)
    //{
    //    Debug.Log(hit.gameObject.name);
    //    if (hit.gameObject.CompareTag("ClimbableWall"))
    //    {
    //        stateMachine.SwitchState(stateMachine.climbingState);
    //    }
    //}

    public override void OnUpdate()
    {
        if (Input.GetMouseButtonDown(0))
        {
            anim.SetTrigger("punchTrigger");
        }
        if (Input.GetMouseButtonDown(1))
        {
            stateMachine.SwitchState(stateMachine.grapplingState);
        }
        if (Input.GetKeyDown(KeyCode.Q))
        {
            stateMachine.SwitchState(stateMachine.grapplingState);
        }
        // pfeil nach unten schaltet collider aus
        if (Input.GetAxis("Vertical") < 0)
        {
            Ray ray = new Ray(transform.position + Vector3.up, Vector3.down);
            RaycastHit hitInfo;
            if (Physics.Raycast(ray, out hitInfo, 1.1f, mask, QueryTriggerInteraction.Ignore))
            {
                Physics.IgnoreCollision(GetComponent<CharacterController>(), hitInfo.collider);
            }
        }
        // movement walk & run, rotation
        float xInput = Input.GetAxis("Horizontal");
        Move(xInput);
        if (xInput > 0) { meshTransform.rotation = Quaternion.Euler(Vector3.up * 90f); }
        else if (xInput < 0) { meshTransform.rotation = Quaternion.Euler(Vector3.up * -90f); }
        // movement jump
        if (jumpCounter > 0 && cc.isGrounded)
        {
            anim.SetBool("isJumping", false);
            jumpCounter = 0;
        }
        if (jumpCounter == 0 && !cc.isGrounded && !isFalling)
        {
            isFalling = true;
            anim.SetBool("isFalling", true);
        }
        if (isFalling && cc.isGrounded)
        {
            isFalling = false;
            anim.SetBool("isFalling", false);
        }
        //if (Input.GetButtonDown("Jump") && (cc.isGrounded || jumpCounter < 2))
        if (Input.GetButtonDown("Jump") && (jumpCounter < 2))
        {
            //Debug.Log("jump!");
            anim.SetBool("isJumping", true);
            jumpCounter++;
            stateMachine.yVelocity = jumpHeight;
        }
        Ray wallRay = new Ray(meshTransform.position + Vector3.up * 0.25f, meshTransform.forward);
        RaycastHit wallHit;
        if (Physics.Raycast(wallRay, out wallHit, 0.6f) && wallHit.collider.CompareTag("ClimbableWall"))
        {
            isTouchingClimbable = true;
        }
        else
        {
            isTouchingClimbable = false;
        }
        if (Input.GetKey(KeyCode.LeftShift) && isTouchingClimbable)
        {
            stateMachine.SwitchState(stateMachine.climbingState);
        }
    }

    void Move(float xInput)
    {
        // compute x velo
        stateMachine.xVelocity += xInput * xAccel * Time.deltaTime;
        stateMachine.xVelocity = Mathf.Clamp(stateMachine.xVelocity, -xSpeedMax, xSpeedMax);
        // compute y velo
        if (!cc.isGrounded)
        {
            stateMachine.yVelocity -= gravity * Time.deltaTime;
        }
        stateMachine.yVelocity = Mathf.Clamp(stateMachine.yVelocity, -terminalY, ySpeedMax);
        // compute velo and move cc
        Vector3 velocity = new Vector3(stateMachine.xVelocity, stateMachine.yVelocity, 0f) * Time.deltaTime;
        cc.Move(velocity);
        // x friction
        stateMachine.xVelocity -= stateMachine.xVelocity * xFriction * Time.deltaTime;
        anim.SetFloat("movespeed", stateMachine.xVelocity);
    }

    public override void OnEnter()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    public override void OnExit()
    {
    }

    //private void OnGUI()
    //{
    //    GUILayout.Label(xVelocity.ToString(), style);
    //    GUILayout.Label(yVelocity.ToString(), style);
    //}
}
