using UnityEngine;

public class CharacterStateMachine : MonoBehaviour
{
    public CharacterState jumpNRunState;
    public CharacterState grapplingState;
    public CharacterState climbingState;
    public CharacterState grapplingPull;
    CharacterState currentState;
    public float xVelocity;
    public float yVelocity;

    void Awake()
    {
        jumpNRunState = GetComponent<JumpNRun>();
        grapplingState = GetComponent<GrapplingAim>();
        climbingState = GetComponent<Climbing>();
        grapplingPull = GetComponent<GrapplingPull>();
    }
        
    void Start()
    {
        SwitchState(jumpNRunState);
    }

    void Update()
    {
        currentState.OnUpdate();
    }

    public void SwitchState(CharacterState state)
    {
        currentState?.OnExit();
        currentState = state;
        currentState?.OnEnter();
    }
}
