using UnityEngine;

public class Climbing : CharacterState
{
    public float climbingSpeed = 4f;
    ItemPickUp itemPickUp;

    protected override void Awake()
    {
        base.Awake();
        itemPickUp = GetComponent<ItemPickUp>();
    }

    public override void OnEnter()
    {
        anim.SetBool("isClimbing", true);
        Renderer[] rends = itemPickUp.itemPoint.GetComponentsInChildren<Renderer>();
        for (int i = 0; i < rends.Length; i++)
        {
            rends[i].enabled = false;
        }
    }

    public override void OnExit()
    {
        anim.SetBool("isClimbing", false);
        anim.SetFloat("climbingspeed", 0);
        Renderer[] rends = itemPickUp.itemPoint.GetComponentsInChildren<Renderer>();
        for (int i = 0; i < rends.Length; i++)
        {
            rends[i].enabled = true;
        }
    }

    public override void OnUpdate()
    {
        if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            stateMachine.SwitchState(stateMachine.jumpNRunState);
        }
        // TODO: climbing anim
        float yInput = Input.GetAxis("Vertical");
        Ray wallRay = new Ray(meshTransform.position + Vector3.up * 0.25f, meshTransform.forward);
        RaycastHit wallHit;
        if (!(Physics.Raycast(wallRay, out wallHit, 0.6f) && wallHit.collider.CompareTag("ClimbableWall")))
        {
            if (yInput > 0)
            {
                yInput = 0f;
            }
            //stateMachine.SwitchState(stateMachine.jumpNRunState);
        }

        // TODO: xVelocity still does not work
        if (Input.GetButtonDown("Jump"))
        {
            stateMachine.xVelocity = -meshTransform.forward.x * 10;
            stateMachine.yVelocity = 8;
            stateMachine.SwitchState(stateMachine.jumpNRunState);
        }
        anim.SetFloat("climbingspeed", yInput);
        cc.Move(Vector3.up * yInput * climbingSpeed * Time.deltaTime);
    }
}
