using UnityEngine;

public class GrapplingAim : CharacterState
{
    public Texture2D cursorTexture;
    public float gravity = 0.2f;
    public float friction = 10f;
    public LayerMask mask;
    public LineRenderer rope;
    public Transform hook;
    bool isGrounded;
    //Vector3 velocity;

    public override void OnEnter()
    {
        Vector2 hotspot = new Vector2(cursorTexture.width * 0.5f, cursorTexture.height * 0.5f);
        Cursor.SetCursor(cursorTexture, hotspot, CursorMode.Auto);
        Cursor.lockState = CursorLockMode.Confined;
        Cursor.visible = true;
    }

    public override void OnExit()
    {
    }

    public override void OnUpdate()
    {
        //if (Input.GetKeyDown(KeyCode.Q))
        //{
        //    stateMachine.SwitchState(stateMachine.jumpNRunState);
        //}
        // gravity
        if (cc.isGrounded && !isGrounded)
        {
            isGrounded = true;
            anim.SetFloat("movespeed", 0f);
            anim.SetBool("isFalling", false);
            anim.SetBool("isJumping", false);
        }
        stateMachine.yVelocity -= gravity * Time.deltaTime;
        stateMachine.yVelocity = Mathf.Clamp(stateMachine.yVelocity, -5f, 0f);
        cc.Move(Vector3.up * stateMachine.yVelocity * Time.deltaTime);
        //velocity -= Vector3.up * gravity * Time.deltaTime;
        //Vector3.ClampMagnitude(velocity, gravity);
        //cc.Move(velocity * Time.deltaTime);
        //velocity -= velocity * friction * Time.deltaTime;

        // raycast von kamera richtung mouse cursor => punkt auf einer plane, die zur kamera zeigt und auf z=0 liegt
        Plane plane = new Plane(Vector3.back, 0f);
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        float enter = 0f;
        Vector3 pointOnPlane = Vector3.zero;
        if (plane.Raycast(ray, out enter))
        {
            pointOnPlane = ray.origin + ray.direction * enter;
        }
        //Debug.DrawLine(transform.position, pointOnPlane, Color.red);
        // raycast aus der mitte des players in richtung dieses punktes
        Vector3 dir = pointOnPlane - meshTransform.position;
        //Debug.DrawRay(meshTransform.position, dir);
        ray = new Ray(meshTransform.position, dir);
        RaycastHit hitInfo = new RaycastHit();
        if(Physics.Raycast(ray, out hitInfo, dir.magnitude, mask, QueryTriggerInteraction.Ignore))
        {
            rope.enabled = true;
            Vector3[] positions = new[] { meshTransform.position, hitInfo.point };
            rope.SetPositions(positions);
            if (Input.GetMouseButtonDown(1))
            {
                hook.position = hitInfo.point;
                hook.parent = hitInfo.transform;
                stateMachine.SwitchState(stateMachine.grapplingPull);
            }
        }
        else
        {
            rope.enabled = false;
        }
    }
}
