using UnityEngine;

public class Jump2D : MonoBehaviour
{
    public float jumpHeight = 10f;

    void Update()
    {
        if (Input.GetButtonDown("Jump"))
        {
            GetComponent<Rigidbody2D>().AddForce(Vector2.up * jumpHeight, ForceMode2D.Impulse);
        }
    }
}
