using UnityEngine;

public class OneWayCollider : MonoBehaviour
{
    public BoxCollider boxCol;

    void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag("Player")) { return; }
        Physics.IgnoreCollision(boxCol, other);
    }

    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player") == false) { return; }
        Physics.IgnoreCollision(boxCol, other, false);
    }
}
