using System;
using UnityEngine;

public class PullablePlatform : MonoBehaviour
{
    public Vector3 endPos;
    public float moveDuration;
    public AnimationCurve moveCurve;
    public event Action onFinishedMoving;
    bool isMoving;
    Vector3 startPos;
    float timer;


    void Update()
    {
        if (!isMoving) { return; }
        if (timer <= moveDuration)
        {
            timer += Time.deltaTime;
            float t = timer / moveDuration;
            t = moveCurve.Evaluate(t);
            transform.position = Vector3.Lerp(startPos, endPos, t);
        }
        else
        {
            isMoving = false;
            timer = 0f;
            onFinishedMoving?.Invoke();
        }
    }

    public void SetMoving(bool moving)
    {
        startPos = transform.position;
        isMoving = moving;
    }
}
