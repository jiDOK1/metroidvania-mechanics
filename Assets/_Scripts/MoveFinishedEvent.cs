using System;
using UnityEngine;

public class MoveFinishedEvent : MonoBehaviour
{
    public event Action onFinishedMoving;

    public void TriggerEvent()
    {
        onFinishedMoving?.Invoke();
    }
}
