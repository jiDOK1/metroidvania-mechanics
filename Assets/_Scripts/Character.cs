using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
    public LayerMask mask;
    public float gravity = 9.81f;
    public float xSpeed = 30f;
    public float xFriction = 20f;
    public float jumpHeight = 5f;
    CharacterController cc;
    Vector3 velocity;
    int jumpCounter;

    void Awake()
    {
        cc = GetComponent<CharacterController>();
    }

    void Update()
    {
        // pfeil nach unten schaltet collider aus
        if(Input.GetAxis("Vertical") < 0)
        {
            Ray ray = new Ray(transform.position + Vector3.up, Vector3.down);
            RaycastHit hitInfo;
            if (Physics.Raycast(ray, out hitInfo, 1.1f, mask, QueryTriggerInteraction.Ignore))
            {
                Physics.IgnoreCollision(GetComponent<CharacterController>(), hitInfo.collider);
            }
        }
        // movement walk & run
        float xVelocity = Input.GetAxis("Horizontal");
        velocity += Vector3.right * xVelocity * xSpeed * Time.deltaTime;
        Vector3.ClampMagnitude(velocity, xSpeed);
        // movement jump
        if (Input.GetButtonDown("Jump") && (cc.isGrounded || jumpCounter < 2))
        {
            jumpCounter++;
            velocity += Vector3.up * jumpHeight;
        }
        // gravity
        velocity -= Vector3.up * gravity * Time.deltaTime;
        Vector3.ClampMagnitude(velocity, gravity);
        cc.Move(velocity);
        velocity -= velocity * xFriction * Time.deltaTime;
        if(jumpCounter > 0 && cc.isGrounded)
        {
            jumpCounter = 0;
        }
    }
}
