using System.Collections;
using UnityEngine;

public class ItemPickUp : MonoBehaviour
{
    public Transform itemPoint;
    Transform meshTransform;
    BoxCollider trigger;
    Rigidbody item;

    void Awake()
    {
        meshTransform = transform.Find("Capsule");
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.T))
        {
            trigger.enabled = false;
            StartCoroutine(TurnOnTriggerCO());
            //item.transform.position = meshTransform.position + meshTransform.forward * 1.5f;
            item.transform.parent = null;
            item.isKinematic = false;
            item.AddForce((meshTransform.forward + Vector3.up) * 5f, ForceMode.Impulse);
            item = null;
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Item"))
        {
            trigger = other.GetComponent<BoxCollider>();
            // TODO Bug
            item = other.transform.parent.GetComponent<Rigidbody>();
            if(item == null) { return; }
            item.isKinematic = true;
            other.transform.parent.position = itemPoint.position;
            other.transform.parent.rotation = itemPoint.rotation;
            other.transform.parent.parent = itemPoint;
        }
    }

    IEnumerator TurnOnTriggerCO()
    {
        yield return new WaitForSeconds(1f);
        trigger.enabled = true;
        trigger = null;
    }
}
