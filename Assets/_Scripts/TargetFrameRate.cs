using UnityEngine;

public class TargetFrameRate : MonoBehaviour
{
    public int targetFPS = 60;

    void OnEnable()
    {
        Application.targetFrameRate = targetFPS;
    }

}
