// brauchen wir fuer IEnumerator
using System.Collections;
using UnityEngine;

public class CoroutineDemo : MonoBehaviour
{
    public float delay = 2f;
    public float duration1 = 5f;
    public float duration2 = 1.5f;

    void Start()
    {
        //StartCoroutine(TestCO());
        //StartCoroutine(LerpCO());
    }

    void Update()
    {
        if (Input.GetButtonDown("Jump"))
        {
            StopAllCoroutines();
            StartCoroutine(LerpCO(0.5f));
        }
    }

    IEnumerator TestCO()
    {
        Debug.Log("starting coroutine");
        // delay
        yield return new WaitForSeconds(delay);
        Debug.Log($"{delay} seconds have passed");
        // delay
        yield return new WaitForSeconds(delay);
        Debug.Log($"{delay} seconds have passed");
        float timer = 0f;
        // sozusagen temporaere Update
        while (timer <= duration1)
        {
            timer += Time.deltaTime;
            transform.localScale += Time.deltaTime * Vector3.one;
            // yield return null heisst einen Loop der Schleife pro Frame ausfuehren (wie Update)
            yield return null;
        }
        // hier geht es weiter, wenn timer > duration1 ist
        // delay
        yield return new WaitForSeconds(delay);
        timer = 0f;
        // zweite temporaere Update
        while (timer <= duration2)
        {
            timer += Time.deltaTime;
            transform.localScale -= Time.deltaTime * Vector3.one;
            yield return null;
        }
        Debug.Log("end of coroutine");
    }

    IEnumerator LerpCO(float startDelay)
    {
        yield return new WaitForSeconds(startDelay);
        float timer = 0f;
        //Vector3 startScale = Vector3.one;
        Vector3 startScale = transform.localScale;
        Vector3 endScale = new Vector3(10, 5, 5);
        Vector3 finalScale = Vector3.one;
        while (timer <= duration1)
        {
            timer += Time.deltaTime;
            float t = timer / duration1;
            transform.localScale = Vector3.Lerp(startScale, endScale, t);
            yield return null;
        }
        timer = 0f;
        while (timer <= duration2)
        {
            timer += Time.deltaTime;
            float t = timer / duration2;
            transform.localScale = Vector3.Lerp(endScale, finalScale, t);
            yield return null;
        }
    }
}
