using System.Collections;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public float morphDuration = 1.5f;
    public float smoothTime = 0.3f;
    public Vector2 xMinMax;
    public Vector2 yMinMax;
    Transform cameraTarget;
    Vector3 velocity = Vector3.zero;

    void Awake()
    {
        cameraTarget = GameObject.Find("CameraTarget").transform;
        CameraZone.onEnteredCameraZone += SetClampValues;
    }

    void Update()
    {
        Vector3 pos = Vector3.SmoothDamp(transform.position, cameraTarget.position, ref velocity, smoothTime);
        float xClamped = Mathf.Clamp(pos.x, xMinMax.x, xMinMax.y);
        float yClamped = Mathf.Clamp(pos.y, yMinMax.x, yMinMax.y);
        transform.position = new Vector3(xClamped, yClamped, pos.z);
    }

    public void SetClampValues(Vector2 xMinMax, Vector2 yMinMax, float morphDuration)
    {
        //this.xMinMax = xMinMax;
        //this.yMinMax = yMinMax;
        this.morphDuration = morphDuration;
        StartCoroutine(MorphClampValuesCO(xMinMax, yMinMax));
    }

    IEnumerator MorphClampValuesCO(Vector2 newXMinMax, Vector2 newYMinMax)
    {
        Vector2 oldXMinMax = xMinMax;
        Vector2 oldYMinMax = yMinMax;
        float morphTimer = 0f;
        while (morphTimer <= morphDuration)
        {
            morphTimer += Time.deltaTime;
            float t = morphTimer / morphDuration;
            xMinMax = Vector2.Lerp(oldXMinMax, newXMinMax, t);
            yMinMax = Vector2.Lerp(oldYMinMax, newYMinMax, t);
            yield return null;
        }
        //yield return new WaitForSeconds(2f);
        //Debug.Log("2 Seconds have passed");
        //yield return null;
        //Debug.Log("one frame has passed");
    }
}
