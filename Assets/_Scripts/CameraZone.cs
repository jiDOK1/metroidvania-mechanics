using System;
using UnityEngine;

public class CameraZone : MonoBehaviour
{
    public Vector2 xMinMax;
    public Vector2 yMinMax;
    public float morphDuration = 0.5f;
    public static Action<Vector2, Vector2, float> onEnteredCameraZone;

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            onEnteredCameraZone?.Invoke(xMinMax, yMinMax, morphDuration);
            //Debug.Log($"entered camerazone {gameObject.name}");
        }
    }
}
